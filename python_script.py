import sys
import sqlite3
from datetime import datetime
import jenkins

db_name = 'data'
table_name = 'history'
args = sys.argv
connection = sqlite3.connect(db_name)
cursor = connection.cursor()

sql = 'create table if not exists %s (job text, color text,timestamp text)' % (
    table_name)
cursor.execute(sql)

server = jenkins.Jenkins(args[1], args[2], args[3])
jobs = server.get_jobs()
for job in jobs:
    print(job['name'], "\t",job['color'])
    sql = "insert into %s VALUES ('%s', '%s', '%s')"%(table_name, job['name'], job['color'], datetime.now())
    cursor.execute(sql)

connection.commit()
connection.close()
